module example.com/m/v2

go 1.16

require (
	github.com/aws/aws-sdk-go v1.8.30
	github.com/checkr/s3-sync v0.0.0-20170612234117-aaa75ae65493
	github.com/fsnotify/fsnotify v1.4.3-0.20170329110642-4da3e2cfbabc // indirect
	github.com/go-ini/ini v1.27.3-0.20170519023713-afbc45e87f3b // indirect
	github.com/hashicorp/hcl v0.0.0-20170217164738-630949a3c5fa // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20160803190731-bd40a432e4c7 // indirect
	github.com/magiconair/properties v1.7.3-0.20170321093039-51463bfca257 // indirect
	github.com/mitchellh/go-homedir v0.0.0-20161203194507-b8bc1bf76747
	github.com/mitchellh/mapstructure v0.0.0-20170307201123-53818660ed49 // indirect
	github.com/pelletier/go-buffruneio v0.2.0 // indirect
	github.com/pelletier/go-toml v0.5.1-0.20170329214941-e32a2e047442 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/spf13/afero v0.0.0-20170217164146-9be650865eab // indirect
	github.com/spf13/cast v1.0.0 // indirect
	github.com/spf13/cobra v0.0.0-20170314171253-7be4beda01ec
	github.com/spf13/jwalterweatherman v0.0.0-20170109133355-fa7ca7e836cf // indirect
	github.com/spf13/pflag v0.0.0-20170327141344-d16db1e50e33 // indirect
	github.com/spf13/viper v0.0.0-20170315134309-84f94806c67f
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/wwkeyboard/bucketPolicyizer v0.0.0-20170530205200-916588924750
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.0.0-20170208141851-a3f3340b5840 // indirect
)
